# SYNC|FRAMEWORK

SYNC|Framework official repository. Are you interested to learn more? Check out the website
http://sync.actt.io

## Docker Image

This is the command to create the latest image of this project.

```
docker build -t acttio/sync:latest .
```

## Contributors

If you want to contribute to this project, clone it and start send your changes to the group.
Today this project has been maintained basically for one person but still help/support are very welcomed!

Donation is the another way to help us on this project. Send us an email if you want to do that.
